package service;

import java.util.List;

import dao.QuizDao;
import dao.UserDao;
import domain.Quiz;
import domain.User;
import util.SecurityUtil;

public class UserService {

	private UserDao userDao;

	public UserService(UserDao userDao) {
		this.userDao = userDao;
	}

	public void create(User user) {
		user.setPassword(SecurityUtil.hashPassword(user.getPassword()));
		userDao.save(user);
	}

	public void update(User user) {
		QuizService quizService = new QuizService(new QuizDao());

		User currentUser = findByUsername(user.getUsername());
		List<Quiz> existingQuizzes = quizService.findByUser(user.getUsername());
		removeByUsername(user.getUsername());
		create(user);

		for(int i=0; i<existingQuizzes.size(); i++){
			existingQuizzes.get(i).setCreatedBy(user);
			quizService.create(existingQuizzes.get(i));
		}
				
	}

	public List<User> findAll() {
		return userDao.findAll();
	}

	public User findByUsername(String username) {
		return userDao.findByUsername(username);
	}

	public User authenticate(String username, String password) {

		User user = findByUsername(username);
		
		System.out.println("auth");

		if (user == null) {
			return null;
		}

		if (SecurityUtil.checkPassword(password, user.getPassword())) {
			user.setPassword("");
			return user;
		}

		return null;
	}

	public void removeByUsername(String username) {
		userDao.removeByUsername(username);
	}
}
