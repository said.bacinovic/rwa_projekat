package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import dao.QuizDao;
import domain.Quiz;
import domain.User;
import service.QuizService;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(description = "Admin Add New Quiz", urlPatterns = { "/admin/add-quiz" })
public class AdminAddNewQuizServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	public AdminAddNewQuizServlet() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("/admin/admin_add_quiz.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String quiz = request.getParameter("quiz");
		Gson gson = new Gson();
		
		Quiz q = gson.fromJson(quiz, Quiz.class);
		
		System.out.println("title"+q.getTitle());
		System.out.println("des"+q.getDescription());
		System.out.println("image"+q.getImageUrl());
		System.out.println("is ac"+q.isActive());
		System.out.println("question color" + q.getQuestionColorscheme());
		
		System.out.println("questions" + q.getQuestions().size());
		
		System.out.println("question 1 " + q.getQuestions().get(0).getTime());
		System.out.println("question size " + q.getQuestions().get(0).getAnswers().get(0).getText());
		System.out.println("question size " + q.getQuestions().get(0).getAnswers().get(0).isCorrect());
		
		
		for(int i=0; i<q.getQuestions().size(); i++){
			for(int j=0; j<q.getQuestions().get(i).getAnswers().size(); j++) {
				if(q.getQuestions().get(i).getAnswers().get(j).getText().equals("")) {
					q.getQuestions().get(i).getAnswers().remove(j);
					j--;
				}
			}
		}
		
		q.setCreatedBy(((User)request.getSession().getAttribute("user")));
		
		QuizService quizService = new QuizService(new QuizDao());
		
		quizService.create(q);
		
		System.out.println("answers " + q.getQuestions().get(0).getAnswers().size());
//		System.out.println("answers text" + q.getQuestions().get(0).getAnswers().get(3).getText() + "nothing");

		
	}
	
}
