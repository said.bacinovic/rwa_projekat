package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import dao.QuizDao;
import domain.Quiz;
import service.QuizService;


/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(description = "Front page", urlPatterns = { "/front-page" })
public class FrontPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	public FrontPageServlet() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("/front-page.html").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	

	}
}