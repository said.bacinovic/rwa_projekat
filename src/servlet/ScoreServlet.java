package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import dao.QuizDao;
import dao.ResultDao;
import domain.Quiz;
import domain.Result;
import service.QuizService;
import service.ResultService;


/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(description = "Score servlet", urlPatterns = { "/score-servlet" })
public class ScoreServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	public ScoreServlet() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String quizId = request.getParameter("quizId");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String email = request.getParameter("email");
		String result = request.getParameter("result");
		
		QuizService quizService = new QuizService(new QuizDao());
		Quiz q = quizService.findById(Integer.parseInt(quizId));
		
		ResultService resultService = new ResultService(new ResultDao());
		Result userResult = new Result(firstName, lastName, email, q.getTitle(), q.getCreatedBy().getUsername(), Integer.parseInt(result));
		
		System.out.println("score " + userResult.getFirstName());
		resultService.create(userResult);
	

	}
}