package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import dao.QuizDao;
import domain.Quiz;
import service.QuizService;


/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(description = "Random quiz", urlPatterns = { "/two-random-quizzes" })
public class TwoRandomQuizzesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	public TwoRandomQuizzesServlet() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("/two-random-quizzes.jsp").forward(request, response);
	}

	
}