package servlet;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import constants.Role;
import dao.UserDao;
import domain.User;
import service.UserService;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(description = "Admin All Users", urlPatterns = { "/admin/all-users" })
public class AdminAllUsersServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	public AdminAllUsersServlet() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("/admin/all-users.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
			
		UserService userService = new UserService(new UserDao());
		String method = request.getParameter("method");
		String username = request.getParameter("username");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String password = request.getParameter("password");
		String role = request.getParameter("role");
		
		System.out.println(firstName);

		
		if(method.equals("delete")) {
			System.out.println("deleteing" + request.getParameter("username"));
			userService.removeByUsername(request.getParameter("username"));
		}
		
		if(method.equals("add")) {
			if(userService.findByUsername(username) == null){
				User newUser = new User();
				newUser.setFirstName(firstName);
				newUser.setLastName(lastName);
				newUser.setPassword(password);
				newUser.setUsername(username);
				
				Role userRole = null;
				
				if(role.equals("admin")){
					userRole = Role.ADMIN;
				} else if (role.equals("editor")){
					userRole = Role.EDITOR;
				}
				
				newUser.setRole(userRole);
				
				
				userService.create(newUser);
			}
		}
		
		if(method.equals("edit")) {
			System.out.println("editing");
			
			if(userService.findByUsername(username) != null){
				User newUser = new User();
				newUser.setFirstName(firstName);
				newUser.setLastName(lastName);
				newUser.setPassword(password);
				System.out.println(userService.findByUsername(username).getPassword());
				newUser.setUsername(username);
				
				Role userRole = null;
				
				if(role.equals("admin")){
					userRole = Role.ADMIN;
				} else if (role.equals("editor")){
					userRole = Role.EDITOR;
				}
				
				newUser.setRole(userRole);
				
				
				userService.update(newUser);
			}
		}
	}
	
	
	
}
