package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import domain.User;
import service.UserService;

import com.google.gson.*;

import constants.Role;

@WebServlet(description = "Servlet for adding new users", urlPatterns = { "/admin/add-user" })
public class AdminAddNewUserServlet extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UserService userService;

	public AdminAddNewUserServlet() {
		super();
		userService = new UserService(new UserDao());
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
			request.getRequestDispatcher("/admin/add-user.jsp").forward(request, response);
			return;
	}
	
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
//		
//		String userUsername = request.getParameter("userUsername");
//		String userFirstName = request.getParameter("userFirstName");
//		String userLastName = request.getParameter("userLastName");
//		String userPassword = request.getParameter("userPassword");
//		String newUserRoles = request.getParameter("userRoles");
//
//		if(userUsername == null || userFirstName == null || userLastName == null || userPassword == null){
//			request.getRequestDispatcher("/admin/addNewUser.jsp").forward(request, response);
//			return;
//		}
//		
//		if(userService.findByUsername(userUsername) == null){
//			User user = new User();
//			
//			user.setFirstName(userFirstName);			
//			user.setLastName(userLastName);
//			user.setUsername(userUsername);
//			user.setPassword(userPassword);
//			
//			List<Role> newRolesForUser = new ArrayList<>();
//			RoleService roleService = new RoleService(new RoleDao());
//			String[] userRoles = newUserRoles.split(",");
//			
//			for(String i : userRoles){
//				if(roleService.findByName(i) != null){
//					newRolesForUser.add(roleService.findByName(i));
//				}
//			}
//			
//			user.setRoles(newRolesForUser);
//			
//			userService.create(user);
		
		String firstName = request.getParameter("first_name");
		String lastName = request.getParameter("last_name");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String role = request.getParameter("role");
		
		if(firstName == null || lastName == null || username == null || password == null || role == null){
			System.out.println("null");
			request.getRequestDispatcher("/admin/add-user.jsp").forward(request, response);
			return;
		}
		
		Role userRole;
		
		if(role.equals("admin")){
			userRole = Role.ADMIN;
		} else if (role.equals("editor")){
			userRole = Role.EDITOR;
		} else {
			request.getRequestDispatcher("/admin/add-user.jsp").forward(request, response);
			return;
		}
		
		if(userService.findByUsername(username) == null){
			System.out.println("adding user");
			User user = new User(username, firstName, lastName, password, userRole);
			userService.create(user);
			request.getRequestDispatcher("/admin/add-user.jsp").forward(request, response);
			return;
		}
			

	}
}
