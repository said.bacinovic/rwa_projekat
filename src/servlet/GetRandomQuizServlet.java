package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import dao.QuizDao;
import domain.Quiz;
import service.QuizService;


/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(description = "Random quiz", urlPatterns = { "/random-quiz" })
public class GetRandomQuizServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	public GetRandomQuizServlet() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		QuizService quizService = new QuizService(new QuizDao());
		Quiz randomQuiz = quizService.findRandom();
		System.out.println("getting random" + randomQuiz.getTitle());
		request.getRequestDispatcher("/single-quiz.jsp?quiz-id=" + randomQuiz.getId()).forward(request, response);
	}

	
}