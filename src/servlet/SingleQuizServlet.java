package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import dao.QuizDao;
import domain.Quiz;
import service.QuizService;


/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(description = "Single quiz", urlPatterns = { "/single-quiz" })
public class SingleQuizServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	public SingleQuizServlet() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("/all-quizzes").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		QuizService quizService = new QuizService(new QuizDao());
		
		String quizId = request.getParameter("quizId");
		Quiz quiz = quizService.findById(Integer.parseInt(quizId));
		
		response.setContentType("application/json");
		PrintWriter pw = response.getWriter();
		
		Gson gson = new Gson();
		System.out.println(quiz.getId());
		pw.println("{\"numberOfQuestions\":" + quiz.getQuestions().size() + ", \"questions\":" + gson.toJson(quiz.getQuestions()) + "}");

	}
}