package servlet;

import java.io.IOException;
import java.nio.file.attribute.UserPrincipalLookupService;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.*;

import dao.QuizDao;
import domain.Quiz;
import service.QuizService;
/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(description = "Delete Quiz", urlPatterns = { "/admin/delete-quiz" })
public class DeleteQuizServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public DeleteQuizServlet() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String quizTitle = request.getParameter("quiz-title");
		QuizService quizService = new QuizService(new QuizDao());
		quizService.removeByTitle(quizTitle);
		request.getRequestDispatcher("/admin/all-quizzes").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}
	
}
