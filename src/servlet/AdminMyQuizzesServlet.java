package servlet;

import java.io.IOException;
import java.nio.file.attribute.UserPrincipalLookupService;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.*;
/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(description = "Admin My Quizzes", urlPatterns = { "/admin/my-quizzes" })
public class AdminMyQuizzesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public AdminMyQuizzesServlet() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("/admin/my-quizzes.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}
	
}
