package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import domain.User;

final public class UserDao extends AbstractDao {

	public void UserDao() {

	}

	public List<User> findAll() {
		EntityManager em = createEntityManager();
		Query q = em.createQuery("SELECT u FROM User u");
		List<User> resultList = q.getResultList();
		em.close();
		return resultList;
	}

	public User findByUsername(String username) {
		EntityManager em = createEntityManager();
		try {
			Query q = em.createQuery("SELECT u FROM User u WHERE u.username = :username").setParameter("username",
					username);
			User user = (User) q.getSingleResult();
			return user;
		} catch (RuntimeException e) {
			System.out.println(e.getMessage());
		} finally {
			if (em != null) {
				em.close();
			}
		}
		return null;
	}

	public void save(User user) {
		EntityManager em = createEntityManager();
		em.getTransaction().begin();
		em.persist(user);
		em.getTransaction().commit();
		em.close();
	}

	public void update(User user) {
		User currentUser = findByUsername(user.getUsername());
		EntityManager em = createEntityManager();
		em.getTransaction().begin();
//		currentUser = user;
		currentUser.setFirstName(user.getFirstName());
		em.getTransaction().commit();
		em.close();
		System.out.println(findByUsername(user.getUsername()).getFirstName());
	}

	public void removeByUsername(String username) {
		EntityManager em = createEntityManager();
		try {
			User userToRemove = findByUsername(username);
			if (userToRemove != null) {
				em.getTransaction().begin();
				User mergedUser = em.merge(userToRemove);
				em.remove(mergedUser);
				em.getTransaction().commit();
			}
		} catch (RuntimeException e) {
			System.out.println(e.getMessage());
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}
}