package constants;

public enum Color {
	BLUE,
	GREY,
	GREEN,
	RED,
	TEAL,
	YELLOW,
	PURPLE,
	ORANGE
}
