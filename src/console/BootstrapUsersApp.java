package console;

import domain.User;
import domain.Answer;
import domain.Question;
import domain.Quiz;

import java.sql.RowIdLifetime;
import java.util.ArrayList;
import java.util.List;

import constants.Role;
import dao.QuizDao;
import dao.ResultDao;
import dao.UserDao;
import service.QuizService;
import service.ResultService;
import service.UserService;

public class BootstrapUsersApp {

	public static void main(String[] args) {

		// UserService userService = new UserService(new UserDao());
		//
		// if (userService.findByUsername("dritchie") == null) {
		//
		// User user = new User();
		//
		// user.setFirstName("Dennis");
		// user.setLastName("Ritchie");
		// user.setUsername("dritchie");
		// user.setPassword("fet.ba");
		//
		// userService.create(user);
		//
		// }
		//
		// if (userService.findByUsername("rpike") == null) {
		//
		// User user = new User();
		//
		// user.setFirstName("Rob");
		// user.setLastName("Pike");
		// user.setUsername("rpike");
		// user.setPassword("fet.ba");
		//
		// userService.create(user);
		//
		// }

		QuizService quizService = new QuizService(new QuizDao());

		UserService userService = new UserService(new UserDao());

		//
		// if(roleService.findByName("admin") == null){
		// Role role = new Role();
		// role.setRoleName("admin");
		//
		// roleService.create(role);
		// }
		//
		// List<Role> allRoles = roleService.findAll();
		//
		// for(Role i:allRoles){
		// System.out.println(i.getRoleName());
		// }
		//
		// System.out.println(allRoles.size());

		if (userService.findByUsername("said") == null) {

			User user = new User();

			user.setFirstName("Admin");
			user.setLastName("Admin");
			user.setUsername("said");
			user.setPassword("admin");
			user.setRole(Role.ADMIN);

			userService.create(user);

		}
		if (userService.findByUsername("admin") == null) {

			User user = new User();

			user.setFirstName("Admin");
			user.setLastName("Admin");
			user.setUsername("admin");
			user.setPassword("admin");
			user.setRole(Role.ADMIN);

			userService.create(user);

		}

		User user = userService.findByUsername("said");
		

		Quiz quiz = new Quiz(user, "quiz 6");
		quiz.setImageUrl("https://pluralsight.imgix.net/paths/path-icons/javascript-36f5949a45.png");
		quiz.setDescription("Javascript quiz");
		quiz.setActive(true);

		List<Answer> answers = new ArrayList<>();
		answers.add(new Answer("a11", false));
		answers.add(new Answer("a21", true));

		Question q1 = new Question("q11", answers, 60, 20);
		Question q2 = new Question("q21", answers, 60, 20);

		quiz.addQuestion(q1);
		quiz.addQuestion(q2);

		if(quizService.findByTitle(quiz.getTitle()) == null){
			System.out.print("aading quiz");
			quizService.create(quiz);
		}
		
		System.out.println(quizService.findAll().size());
		
		for(Quiz q: quizService.findAll()){
			System.out.println(q.getTitle() + "//" + q.getId() + "//" + (q.isActive()==true ? "yes" : "no"));
		}
		
		for(Quiz q: quizService.findByTitleOrDescription("ja")){
			System.out.println(q.getTitle() + "//" + q.getDescription());
		}
		
		ResultService resultService = new ResultService(new ResultDao());
		
		System.out.println("resul " + resultService.findAll().size());
				

	}
}
