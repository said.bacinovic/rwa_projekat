<%@page import="constants.Role"%>
<%@page import="service.QuizService"%>
<%@page import="dao.QuizDao"%>
<%@page import="domain.Quiz"%>
<%@page import="domain.User"%>
<%@page import="service.ResultService"%>
<%@page import="dao.ResultDao"%>
<%@page import="domain.Result"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!doctype html PUBLIC>

<html lang="en">

<head>
    <meta charset="utf-8">

    <title>Admin - Inbox</title>
    <meta name="description" content="Admin - quiz list">
    <meta name="author" content="Said Bacinovic">

    <link rel="stylesheet" type="text/css" href="css/style.css">

    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!-- Material icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>

<body>
<%
    Boolean isAdmin = ((User)request.getSession().getAttribute("user")).getRole() == Role.ADMIN;
%>
    <!-- Side navigation-->
    <ul id="slide-out" class="sidenav">

        <li><a class="subheader">Quizzes</a></li>

        <li><a class="waves-effect" href="all-quizzes">All quizzes</a></li>
        <li><a class="waves-effect" href="my-quizzes">My quizzes</a></li>
        <li><a class="waves-effect" href="add-quiz">Create new quiz</a></li>

        <li>
            <div class="divider"></div>
        </li>
		
		<%= isAdmin ? "<li><a class=\"subheader\">Users</a></li> <li><a class=\"waves-effect\" href=\"all-users\">All users</a></li> <li><a class=\"waves-effect\" href=\"add-user\">Add new user</a></li> " : "" %>
        
        <li>
            <div class="divider"></div>
        </li>
        <li class="active"><a class="waves-effect" href="inbox.jsp">Inbox</a></li>

    </ul>

    <!-- Sidenav button-->
    <a href="#" data-target="slide-out" class="sidenav-trigger"><img src="images/menu.png"></a>
    <a href="/quiz/login?logout=true" class="btn white black-text btn-flat right log-out-button">Log out</a>


    <div class="divider"></div>


    <!-- <a class="waves-effect waves-light btn modal-trigger" href="#modal1">Modal</a> -->




    <!-- Table -->
    <div class="row container">
        <h3>Inbox</h3>
        <table class="highlight striped centered responsive-table">
            <thead>
                <tr>
                    <th>Quiz name</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Result</th>
                </tr>
            </thead>

            <tbody>
            
            
            <%! String printAllResults(String username){
            	
            		ResultService resultService = new ResultService(new ResultDao());
            		java.util.List<Result> results = resultService.findByUsername(username);
					
            		String returnString = new String();
            		
            		for(Result r:results) {
            			returnString += "<tr>" + 	
            				"<td>" + r.getQuiz_title() + "</td>" +
            				"<td>" + r.getFirstName() + "</td>" +
            				"<td>" + r.getLastName() + "</td>" +
            				"<td>" + r.getEmail() + "</td>" +
            				"<td>" + r.getScore() + "</td>" +
            				"</tr>";
            		}
            
            		return returnString;
            	} 
            %>
            
            <%= printAllResults(((User)request.getSession().getAttribute("user")).getUsername()) %>
            
           
            </tbody>
        </table>
    </div>


    <!-- Jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <!-- Material dialog-->
    <script src="material-dialog/material-dialog.min.js" type="text/javascript"></script>

    <script>
        $(document).ready(function () {
            $('select').formSelect();
        });

        $(document).ready(function () {
            $('.modal').modal();
        });


        $(document).ready(function () {
            $('.sidenav').sidenav();
        });
    </script>

</body>

</html>