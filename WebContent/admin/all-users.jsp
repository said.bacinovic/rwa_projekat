<%@page import="constants.Role"%>
<%@page import="service.UserService"%>
<%@page import="dao.UserDao"%>
<%@page import="domain.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!doctype html PUBLIC>

<html lang="en">

<head>
    <meta charset="utf-8">

    <title>Admin - Users</title>
    <meta name="description" content="Admin - Users">
    <meta name="author" content="Said Bacinovic">


    <link rel="stylesheet" type="text/css" href="css/style.css">


    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!-- Material icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


</head>

<body>


<%
    Boolean isAdmin = ((User)request.getSession().getAttribute("user")).getRole() == Role.ADMIN;
%>
    <!-- Side navigation-->
    <ul id="slide-out" class="sidenav">

        <li><a class="subheader">Quizzes</a></li>

        <li><a class="waves-effect" href="all-quizzes">All quizzes</a></li>
        <li><a class="waves-effect" href="my-quizzes">My quizzes</a></li>
        <li><a class="waves-effect" href="add-quiz">Create new quiz</a></li>

        <li>
            <div class="divider"></div>
        </li>
		
		<%= isAdmin ? "<li><a class=\"subheader\">Users</a></li> <li class=\"active\"><a class=\"waves-effect\" href=\"all-users\">All users</a></li> <li><a class=\"waves-effect\" href=\"add-user\">Add new user</a></li> " : "" %>
        <li>
            <div class="divider"></div>
        </li>
        <li class="active"><a class="waves-effect" href="inbox.jsp">Inbox</a></li>
    </ul>


    <!-- Sidenav button-->
    <a href="#" data-target="slide-out" class="sidenav-trigger"><img src="images/menu.png"></a>
    <a href="/quiz/login?logout=true" class="btn white black-text btn-flat right log-out-button">Log out</a>


    <div class="divider"></div>

    <div class="row container">
        <!-- Table -->
            <h3>All Users</h3>
            <table class="highlight striped centered responsive-table">
                <thead>
                    <tr>
                        <th>Username</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Role</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>

                        <%! String printAllUsers(){
            	
                            UserService userService = new UserService(new UserDao());
                            java.util.List<User> users = userService.findAll();
                            
                            String returnString = new String();
                            
                            for(User u:users) {
                                
                                returnString += "<tr>" + 
                                    "<td>" + u.getUsername() + "</td>" +
                                    "<td>" + u.getFirstName() + "</td>" +
                                    "<td>" + u.getLastName() + "</td>" +
                                    "<td>" + u.getRole() + "</td>" +
                                    "<td><a class=\"btn white black-text\" onclick=\"editUser(this)\">Edit</a>" +
                                    "<a class=\"btn red\" onclick=\"deleteUser(this)\">Delete</a></td>" +
                                    "</tr>";
                            }
                    
                            return returnString;
                        } 
                    %>
                    
                    <%= printAllUsers() %>

                </tbody>
            </table>
        </div>


    <!-- Jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <!-- Material dialog-->
    <script src="material-dialog/material-dialog.min.js" type="text/javascript"></script>

    <script>
        $(document).ready(function () {
            $('select').formSelect();
        });
        

        $(document).ready(function () {
            $('.sidenav').sidenav();
        });


        function deleteUser(user){
            var row = user.parentElement.parentElement;

            var username = $(row).children()[0].innerHTML;

            MaterialDialog.dialog(
                    "Are you sure you want to delete user: " + username,
                    {
                        title: "Confirm Delete",
                        buttons: {
                            // Use by default close and confirm buttons
                            close: {
                                className: "transparent black-text",
                                text: "discard",
                                callback: function () {
                                }
                            },
                            confirm: {
                                className: "red",
                                text: "confirm",
                                callback: function () {
                                	 
                                    $.ajax({
                                        url: 'all-users',
                                        type: 'POST',
                                        data: {'username':username, 'method': 'delete'},
                                        success: function(result) {
                                            $(row).remove();
                                        }
                                    });
                                    
                                }
                            }
                        }
                    }
                );
                       
        }
        
        function editUser(user){
        	
        	 var row = user.parentElement.parentElement;

             var username = $(row).children()[0].innerHTML;
             
         	window.location.replace("add-user?username=" + username);

        }
    </script>

</body>

</html>