<%@page import="constants.Role"%>
<%@page import="service.QuizService"%>
<%@page import="dao.QuizDao"%>
<%@page import="domain.Quiz"%>
<%@page import="domain.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!doctype html PUBLIC>

<html lang="en">

<head>
    <meta charset="utf-8">

    <title>Admin - Quiz List</title>
    <meta name="description" content="Admin - quiz list">
    <meta name="author" content="Said Bacinovic">

    <link rel="stylesheet" type="text/css" href="css/style.css">

    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!-- Material icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>

<body>
<%
    Boolean isAdmin = ((User)request.getSession().getAttribute("user")).getRole() == Role.ADMIN;
%>
    <!-- Side navigation-->
    <ul id="slide-out" class="sidenav">

        <li><a class="subheader">Quizzes</a></li>

        <li class="active"><a class="waves-effect" href="all-quizzes">All quizzes</a></li>
        <li><a class="waves-effect" href="my-quizzes">My quizzes</a></li>
        <li><a class="waves-effect" href="add-quiz">Create new quiz</a></li>

        <li>
            <div class="divider"></div>
        </li>
		
		<%= isAdmin ? "<li><a class=\"subheader\">Users</a></li> <li><a class=\"waves-effect\" href=\"all-users\">All users</a></li> <li><a class=\"waves-effect\" href=\"add-user\">Add new user</a></li> " : "" %>
        <li>
            <div class="divider"></div>
        </li>
        <li class="active"><a class="waves-effect" href="inbox.jsp">Inbox</a></li>
    </ul>

    <!-- Sidenav button-->
    <a href="#" data-target="slide-out" class="sidenav-trigger"><img src="images/menu.png"></a>
    <a href="/quiz/login?logout=true" class="btn white black-text btn-flat right log-out-button">Log out</a>


    <div class="divider"></div>


    <!-- <a class="waves-effect waves-light btn modal-trigger" href="#modal1">Modal</a> -->




    <!-- Table -->
    <div class="row container">
        <h3>All quizzes</h3>
        <table class="highlight striped centered responsive-table">
            <thead>
                <tr>
                    <th>Image</th>
                    <th>Quiz Title</th>
                    <th>Created By</th>
                    <th>Active</th>
                    <th>Questions</th>
                    <th>Actions</th>
                </tr>
            </thead>

            <tbody>
            
            
            <%! String printAllQuizzesList(){
            	
            		QuizService quizService = new QuizService(new QuizDao());
            		java.util.List<Quiz> quizzes = quizService.findAll();
					
            		String returnString = new String();
            		
            		for(Quiz q:quizzes) {
            			
            			returnString += "<tr>" + 
            				"<td> <img class=\"responsive-img circle\" src=\"" +
            				q.getImageUrl() + "\"></td>" +
            				"<td>" + q.getTitle() + "</td>" +
            				"<td>" + q.getCreatedBy().getUsername() + "</td>" +
            				"<td>" + q.isActive() + "</td>" +
            				"<td>" + q.getQuestions().size() + "</td>" +
            				"<td><a class=\"btn white black-text\" onclick=\"editQuiz("+q.getId()+")\">Edit</a>" +
            				"<a class=\"btn red\" onclick=\"deleteQuiz('"+q.getTitle()+"')\">Delete</a></td>" +
            				"</tr>";
            		}
            
            		return returnString;
            	} 
            %>
            
            <%= printAllQuizzesList() %>
            
           
            </tbody>
        </table>
    </div>


    <!-- Jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <!-- Material dialog-->
    <script src="material-dialog/material-dialog.min.js" type="text/javascript"></script>

    <script>
        $(document).ready(function () {
            $('select').formSelect();
        });

        function deleteQuiz(quizTitle) {
            MaterialDialog.dialog(
                "You are about to delete quiz: " + quizTitle,
                {
                    title: "Confirm Delete",
                    buttons: {
                        // Use by default close and confirm buttons
                        confirm: {
                            className: "red",
                            text: "confirm",
                            callback: function () {
                            	window.location.href = "/quiz/admin/delete-quiz?quiz-title="+quizTitle;
                            }
                        },
                        close: {
                            className: "transparent black-text",
                            text: "discard",
                            modalClose: false,
                            callback: function () {
                            }
                        }
                    }
                }
            );

        };
        
        function editQuiz(quizId){
        	window.location.href = "/quiz/admin/edit-quiz?quiz-id="+quizId;
        }

        $(document).ready(function () {
            $('.modal').modal();
        });


        $(document).ready(function () {
            $('.sidenav').sidenav();
        });
    </script>

</body>

</html>